#include "hook.h"
#include <Windows.h>

inline DWORD* CVirtualHook::GetTable(LPVOID pInst) {
	return *(DWORD**)pInst;
};

inline DWORD CVirtualHook::GetFunction(LPVOID pInst, int iIndex) {
	return GetTable(pInst)[iIndex];
};

DWORD CVirtualHook::HookFunction(LPVOID pInst, int iIndex, LPVOID pHookFunc) {
	DWORD* pTable = GetTable(pInst);
	DWORD  dwOrg = GetFunction(pInst, iIndex);

	DWORD* dwFunc = &pTable[iIndex];
	DWORD dwProtect = 0;
	VirtualProtect(dwFunc, m_iAddrSize, PAGE_EXECUTE_READWRITE, &dwProtect);
	*dwFunc = (DWORD)pHookFunc;
	VirtualProtect(dwFunc, m_iAddrSize, dwProtect, &dwProtect);

	return dwOrg;
};