/*
*	VHook
*		A VMT Hooking class by Haffy
*/

#pragma once
#include <Windows.h>

class CVirtualHook {
private:
	int m_iAddrSize = sizeof(DWORD);
public:
	DWORD*  GetTable(LPVOID);
	DWORD	GetFunction(LPVOID, int = 0);
	DWORD   HookFunction(LPVOID, int, LPVOID);
}; 
CVirtualHook gHook = CVirtualHook();